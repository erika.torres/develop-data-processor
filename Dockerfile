FROM python:3.7-alpine

ENV PYTHON_PACKAGES="\
    numpy \
    matplotlib \
    scipy \
    scikit-learn \
    pandas\
"

RUN apt-get install -y python3-pip
RUN pip3 install --upgrade pip \
&& pip3 install --no-cache-dir $PYTHON_PACKAGES


RUN python3 -c "import numpy; print(numpy.version.version)"